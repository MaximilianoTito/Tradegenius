                                    --######        EMXSISROLE        ######--

        CREATE SCHEMA auth;

        CREATE TABLE auth.users (
            id SERIAL PRIMARY KEY,
            name VARCHAR(100) NOT NULL,
            username VARCHAR(50) NOT NULL,
            email VARCHAR(100),
            password VARCHAR(100) NOT NULL,
            phone_number VARCHAR(10) NOT NULL,
            home_address VARCHAR(100)
        );

        CREATE TABLE auth.roles (
            id SERIAL PRIMARY KEY,
            name VARCHAR(50) NOT NULL
        );

        CREATE TABLE auth.user_role (
            user_id INT NOT NULL,
            role_id INT NOT NULL,
            PRIMARY KEY (user_id, role_id),
            FOREIGN KEY (user_id) REFERENCES auth.users(id),
            FOREIGN KEY (role_id) REFERENCES auth.roles(id)
        );

        INSERT INTO auth.users (name, username, password, phone_number, home_address) VALUES
        ('admin', 'admin', 'admin', 'admin', 'admin'),
        ('employee', 'employee', 'employee', 'employee', 'employee'),
        ('customer', 'customer', 'customer', 'customer', 'customer');

        INSERT INTO auth.roles (name) VALUES
        ('ADMIN'),
        ('EMPLOYEE'),
        ('CUSTOMER');

        INSERT INTO auth.user_role (user_id, role_id) VALUES 
        (1, 1), 
        (2, 2), 
        (3, 3);

    SELECT u.username, r.name
        FROM auth.user_role ur
        JOIN auth.users u on u.id = ur.user_id
        JOIN auth.roles r on r.id = ur.role_id;

                                    --######        EMXSISSHOP        ######--

        CREATE SCHEMA ecommerce;

        CREATE TABLE ecommerce.products (
            id SERIAL PRIMARY KEY,
            name VARCHAR(100) NOT NULL,
            description TEXT,
            quantity INT,
            price NUMERIC(5,2),
            status BOOLEAN

        );

        CREATE TABLE ecommerce.services (
            id SERIAL PRIMARY KEY,
            name VARCHAR(100) NOT NULL,
            description TEXT,
            price NUMERIC(5,2),
            status BOOLEAN
        );

        CREATE TABLE ecommerce.shopping_cart(
            id SERIAL PRIMARY KEY,
            id_user INT,
            id_product INT,
            id_service INT,
            quantity INT,
            subtotal NUMERIC(5,2),
            FOREIGN KEY(id_user) REFERENCES auth.users(id),
            FOREIGN KEY(id_product) REFERENCES ecommerce.products(id),
            FOREIGN KEY(id_service) REFERENCES ecommerce.services(id)
        );

        CREATE TABLE ecommerce.transactions (
            id SERIAL PRIMARY KEY,
            id_user INT,
            date DATE,
            total NUMERIC(5,2),
            status BOOLEAN,
            FOREIGN KEY(id_user) REFERENCES auth.users(id)
        );