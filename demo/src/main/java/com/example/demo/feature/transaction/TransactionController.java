package com.example.demo.feature.transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/transaction")
@CrossOrigin("*")
public class TransactionController {
    @Autowired
    private TransactionRespository transactionRespository;

    @GetMapping("/findAll")
    public List<Transaction>findAll(){
        return transactionRespository.findAll();
    }
}
