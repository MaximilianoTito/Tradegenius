package com.example.demo.feature.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/service")
public class ServicesController {
    @Autowired
    private ServicesService servicesService;

    @GetMapping("findAll")
    public List<Services>findAll(){
        return servicesService.findAll();
    }
    
}
