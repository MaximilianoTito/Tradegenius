package com.example.demo.feature.service;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ServicesRepository extends JpaRepository<Services, Long>{
    
}
