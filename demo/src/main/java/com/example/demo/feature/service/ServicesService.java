package com.example.demo.feature.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServicesService {
    @Autowired
    private ServicesRepository servicesRepository;

    public List<Services> findAll(){
        return servicesRepository.findAll();
    }

}
