package com.example.demo.feature.transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {
    @Autowired
    private TransactionRespository transactionRespository;

    public List<Transaction> findAll(){
        return transactionRespository.findAll();
    }
}
