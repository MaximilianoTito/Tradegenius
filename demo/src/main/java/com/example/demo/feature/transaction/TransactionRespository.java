package com.example.demo.feature.transaction;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRespository extends JpaRepository<Transaction, Long> {
    
}
