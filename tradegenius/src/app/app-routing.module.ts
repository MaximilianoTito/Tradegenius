  import { NgModule } from '@angular/core';
  import { RouterModule, Routes } from '@angular/router';

  const routes: Routes = [
    {path: '', redirectTo:'emxsis/dashboard', pathMatch: 'full', },
    {path: 'emxsis', loadChildren: ()=> import('./layout/layout.module').then(m=>m.LayoutModule)},
    {path: '**', redirectTo: 'emxsis/dashboard', pathMatch: 'full'},
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
