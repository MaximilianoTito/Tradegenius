import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListShoppingCartComponent } from './list-shopping-cart/list-shopping-cart.component';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingCartComponent } from './shopping-cart.component';

const routes : Routes = [
  {path: '', component: ShoppingCartComponent}
]

@NgModule({
  declarations: [
    ShoppingCartComponent,
    ListShoppingCartComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ShoppingCartModule { }
