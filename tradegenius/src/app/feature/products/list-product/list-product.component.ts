import { Component } from '@angular/core';
import { Product } from '../model/product';
import { ProductService } from '../product.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogAnimationsExampleDialogs } from './dialog-animations-example-dialog';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
})
export class ListProductComponent {
  products: Product[] = [];
  
  constructor(
    private productService: ProductService,
    public dialog: MatDialog
    ) {}

  ngOnInit(): void {
    this.findAll();
  }

  public findAll(): void {
    this.productService.findAll().subscribe(
      (response: Product[]) => {
        this.products = response;
        console.log(response);
      },
      (error) => {
        console.error('Error', error);
      }
    );
  }

  openDialog(enterAnimationDuration: string, exitAnimationDuration: string): void {
    this.dialog.open(DialogAnimationsExampleDialogs, {
      width: '250px',
      enterAnimationDuration,
      exitAnimationDuration,
    });
  }
}

