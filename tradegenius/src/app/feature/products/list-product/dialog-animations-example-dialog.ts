import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-dialog-animations-example-dialog',
  templateUrl: './dialog-animations-example-dialog.html',
})
export class DialogAnimationsExampleDialogs {
  constructor(
    public dialogRef: MatDialogRef<DialogAnimationsExampleDialogs>,
    private matSnackBar: MatSnackBar
  ) {}

    public snackBar (message: string, action: string){
      this.matSnackBar.open(message, action)
    }

}
