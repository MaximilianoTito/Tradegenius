import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products.component';
import { ListProductComponent } from './list-product/list-product.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { DialogAnimationsExampleDialogs } from './list-product/dialog-animations-example-dialog';

const routes: Routes = [
  { path: '', component: ProductsComponent }
];

@NgModule({
  declarations: [
    ProductsComponent,
    ListProductComponent,
    DialogAnimationsExampleDialogs
  ],
  imports: [
    MatSnackBarModule,
    MatButtonModule,
    MatDialogModule,
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ProductsModule { }
