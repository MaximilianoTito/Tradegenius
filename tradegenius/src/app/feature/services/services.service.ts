import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Service } from './model/service';

@Injectable({
  providedIn: 'root',
})
export class ServicesService {
  private url: string = 'http://localhost:8080/api/service';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  public findAll(): Observable<Service[]> {
    return this.http.get<Service[]>(`${this.url}/findAll`, this.httpOptions);
  }
    
}
