import { Component, OnInit } from '@angular/core';
import { Service } from '../model/service';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-list-services',
  templateUrl: './list-services.component.html',
})
export class ListServicesComponent implements OnInit {
  ngOnInit(): void {
    this.findAll();
  }

  constructor(private servicesServicre: ServicesService) {}

  services: Service[] = [];

  public findAll() : void {
    this.servicesServicre.findAll().subscribe((response: Service[]) => {
      this.services = response;
      console.log(response)
    },
    (error) => {
      console.error('Error', error)
    });
  }
}
