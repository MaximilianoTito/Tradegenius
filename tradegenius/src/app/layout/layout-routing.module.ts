import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'products',
        loadChildren: () =>
          import('../feature/products/products.module').then(
            (m) => m.ProductsModule
          ),
      },
      {
        path: 'services',
        loadChildren: () =>
          import('../feature/services/services.module').then(
            (m) => m.ServicesModule
          ),
      },
      {
        path: 'shoppingCart',
        loadChildren: () =>
          import('../feature/shopping-cart/shopping-cart.module').then(
            (m) => m.ShoppingCartModule
          ),
      },
    ],
  },
];
@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutRoutingModule {}
