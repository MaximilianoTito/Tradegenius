import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  @Output() sidenavToggle = new EventEmitter<void>();
  public onSidenav (){
    this.sidenavToggle.emit()
  }
}
